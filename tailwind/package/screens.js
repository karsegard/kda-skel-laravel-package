module.exports = {
    'lgcy': '320px',
    'old': '480px',
    'sm': '576px',
    'md': '768px',
    'lg': '992px',
    'xl': '1200px',
    // '2xl': '1536px',
}