/*
Extension du theme 
*/
module.exports ={
   

    backgroundPosition: {
        
    },
    gridTemplateRows: {
       
    },

   
    spacing: {
        'inherit': 'inherit',
        '1-12': '1.12em',
        '0-65': '0.65em',
    },

    minHeight: {
        
    },

    maxWidth: {
        'desktop': '1140px',
        'larger': '1140px',
    },

    borderWidth: {
        DEFAULT: '1px',
    },

    transitionProperty: {
        'maxheight': 'max-height',
        'width': 'width',
        'height': 'height',
    },
    transitionTimingFunction: {
        'bounce': 'cubic-bezier(0.16, 1.8, 0.56, 0.81)',
    },
    keyframes: {
        rolling: {
            'from': { transform: 'translate3d(0, 0, 0)' },
            'to': { transform: 'translate3d(-50%, 0, 0);' },
        },
        scrolling:{
            'from':  { transform: 'translateX(0);' },
            'to' :{ transform: 'translateX(calc(-1 * var(--marquee-width)));' }
        }
    },
    animation: {
        rolling: 'rolling 20s linear infinite',
        scrolling: 'scrolling var(--marquee-time) linear infinite;',
        
    },

}