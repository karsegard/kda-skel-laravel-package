<?php

namespace VendorName\Skeleton\Facades;

use Illuminate\Support\Facades\Facade;

class FacadeName extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
