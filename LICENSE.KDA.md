Copyright
Copyright 2023 Karsegard Digital Agency Sàrl
Copyright 2023 Fabien Karsegard
Copyright 2023 Emilie Karsegard
Pour plus d'informations, consultez les conditions d'utilisation complètes à l'adresse suivante : https://www.karsegard.ch/conditions.pdf

1. Droits d'utilisation de la librairie
Le Client acquiert de Karsegard Digital Agency Sàrl le droit d’utiliser la librairie fournie par le Prestataire exclusivement pour ses besoins internes, pour le territoire d’implantation du Client au moment de la réalisation et pour toute la durée d'utilisation du Projet. Le Client n'est pas autorisé à reproduire, redistribuer ou adapter cette librairie pour d'autres projets sans une autorisation explicite écrite du Prestataire.

2. Licence d’utilisation du code de la librairie
Karsegard Digital Agency Sàrl accorde au Client une licence d’utilisation incessible et intransmissible du code de la librairie créée pour le Projet et pour le territoire d’implantation du Client au moment de la réalisation. Cette licence permet au Client d'utiliser le code de la librairie uniquement pour ses besoins internes dans le cadre du Projet.

3. Propriété des éléments externes
Les éléments externes utilisés par le Prestataire dans la librairie restent la propriété exclusive de leurs auteurs respectifs. Le Client ne dispose d'aucun droit sur ces éléments à moins d'avoir obtenu les autorisations nécessaires de la part des auteurs concernés.

4. Utilisation non open-source
La librairie fournie sous cette licence n'est pas open-source. Toute utilisation ou distribution de la librairie au-delà du Projet initial nécessite l'acquisition d'une licence supplémentaire de Karsegard Digital Agency Sàrl. Le Client s'engage à respecter les termes de cette licence et à ne pas divulguer le code de la librairie à des tiers.

5. Absence de garantie
La librairie est fournie "en l'état", sans aucune garantie de quelque nature que ce soit, expresse ou implicite, y compris mais sans s'y limiter, les garanties de qualité marchande et d'adéquation à un usage particulier. En aucun cas, Karsegard Digital Agency Sàrl ne pourra être tenu responsable de tout dommage direct, indirect, accessoire, spécial ou consécutif découlant de l'utilisation ou de l'impossibilité d'utiliser la librairie, même si Karsegard Digital Agency Sàrl a été informé de la possibilité de tels dommages.

En utilisant la librairie fournie par Karsegard Digital Agency Sàrl, le Client accepte les termes de cette licence. Pour toute question ou demande d'autorisation supplémentaire, veuillez contacter Karsegard Digital Agency Sàrl.